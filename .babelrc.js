module.exports = {
  presets: ['@babel/preset-env', '@babel/react', '@babel/typescript'],
  plugins: [
    [
      'babel-plugin-styled-components',
      {
        pure: true
      }
    ]
  ]
};
