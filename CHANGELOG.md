## [1.6.17](https://gitlab.com/sparqhub/edge/compare/v1.6.16...v1.6.17) (2020-02-29)


### Bug Fixes

* box-sizing fixes ([bb14c92](https://gitlab.com/sparqhub/edge/commit/bb14c92c43e9854368a86fb73f171c0e1413ac5b))
* fix duplication, fonts loading ([60d7ccf](https://gitlab.com/sparqhub/edge/commit/60d7ccf8e15e6faaca6c3fb752f8ee52b479616b))

## [1.6.16](https://gitlab.com/sparqhub/edge/compare/v1.6.15...v1.6.16) (2020-02-22)


### Bug Fixes

* update all packages ([9de1264](https://gitlab.com/sparqhub/edge/commit/9de1264f5705be20cd7414079f698e97b9af6c78))

## [1.6.15](https://gitlab.com/sparqhub/edge/compare/v1.6.14...v1.6.15) (2019-12-01)


### Bug Fixes

* updated dependencies ([b6f3105](https://gitlab.com/sparqhub/edge/commit/b6f3105293e7292b2c4e37355367b60073905e3e))

## [1.6.14](https://gitlab.com/sparqhub/edge/compare/v1.6.13...v1.6.14) (2019-10-06)


### Bug Fixes

* styled-component peer dependency ([ce9c99d](https://gitlab.com/sparqhub/edge/commit/ce9c99d))

## [1.6.13](https://gitlab.com/sparqhub/edge/compare/v1.6.12...v1.6.13) (2019-09-15)


### Bug Fixes

* **Select:** add value types ([fc16628](https://gitlab.com/sparqhub/edge/commit/fc16628))
* **Select:** cleanup ([a9c89d0](https://gitlab.com/sparqhub/edge/commit/a9c89d0))

## [1.6.12](https://gitlab.com/sparqhub/edge/compare/v1.6.11...v1.6.12) (2019-09-15)


### Bug Fixes

* **Select:** small improvements ([136c03c](https://gitlab.com/sparqhub/edge/commit/136c03c))

## [1.6.11](https://gitlab.com/sparqhub/edge/compare/v1.6.10...v1.6.11) (2019-09-14)


### Bug Fixes

* **Select:** fix selected value not being reflected if changed from the outside ([e854bdc](https://gitlab.com/sparqhub/edge/commit/e854bdc))

## [1.6.10](https://gitlab.com/sparqhub/edge/compare/v1.6.9...v1.6.10) (2019-09-14)


### Bug Fixes

* **ThemeProvider:** pass red and yellow colors to theme ([3ea28b1](https://gitlab.com/sparqhub/edge/commit/3ea28b1))

## [1.6.9](https://gitlab.com/sparqhub/edge/compare/v1.6.8...v1.6.9) (2019-09-13)


### Bug Fixes

* **Select:** revert type changes ([ba14f20](https://gitlab.com/sparqhub/edge/commit/ba14f20))

## [1.6.8](https://gitlab.com/sparqhub/edge/compare/v1.6.7...v1.6.8) (2019-09-13)


### Bug Fixes

* **Select:** genneric value for Select ([ded71a0](https://gitlab.com/sparqhub/edge/commit/ded71a0))

## [1.6.7](https://gitlab.com/sparqhub/edge/compare/v1.6.6...v1.6.7) (2019-09-11)


### Bug Fixes

* **Select:** added clerable prop to enable clear selection button ([c5eab67](https://gitlab.com/sparqhub/edge/commit/c5eab67))

## [1.6.6](https://gitlab.com/sparqhub/edge/compare/v1.6.5...v1.6.6) (2019-09-11)


### Bug Fixes

* **Dropdown:** few style fixes ([1176151](https://gitlab.com/sparqhub/edge/commit/1176151))

## [1.6.5](https://gitlab.com/sparqhub/edge/compare/v1.6.4...v1.6.5) (2019-09-11)


### Bug Fixes

* **Select:** select field now takes only remaining available space ([03bcec3](https://gitlab.com/sparqhub/edge/commit/03bcec3))

## [1.6.4](https://gitlab.com/sparqhub/edge/compare/v1.6.3...v1.6.4) (2019-09-10)


### Bug Fixes

* **Icon:** revert changes ([83565d7](https://gitlab.com/sparqhub/edge/commit/83565d7))
* **Option:** seleted state ([0fb6e29](https://gitlab.com/sparqhub/edge/commit/0fb6e29))

## [1.6.3](https://gitlab.com/sparqhub/edge/compare/v1.6.2...v1.6.3) (2019-09-10)


### Bug Fixes

* **Icon:** update icon setup ([cfc8d7f](https://gitlab.com/sparqhub/edge/commit/cfc8d7f))

## [1.6.2](https://gitlab.com/sparqhub/edge/compare/v1.6.1...v1.6.2) (2019-09-05)


### Bug Fixes

* **CategoryPicker:** spread props on container element ([441f2fd](https://gitlab.com/sparqhub/edge/commit/441f2fd))
* **Menu:** added z-index ([8a726d2](https://gitlab.com/sparqhub/edge/commit/8a726d2))
* **Modal:** added z-index property ([4fed9d1](https://gitlab.com/sparqhub/edge/commit/4fed9d1))

## [1.6.1](https://gitlab.com/sparqhub/edge/compare/v1.6.0...v1.6.1) (2019-09-05)


### Bug Fixes

* styled-components version ([461ca30](https://gitlab.com/sparqhub/edge/commit/461ca30))

# [1.6.0](https://gitlab.com/sparqhub/edge/compare/v1.5.8...v1.6.0) (2019-09-04)


### Bug Fixes

* **Modal:** close on Esc ([f4cb1c9](https://gitlab.com/sparqhub/edge/commit/f4cb1c9))
* **Modal:** fix overflow for long content ([de9fc9b](https://gitlab.com/sparqhub/edge/commit/de9fc9b))
* add bordered hidden prop to Option ([60cb5e4](https://gitlab.com/sparqhub/edge/commit/60cb5e4))
* update dependencies ([a5c1c2c](https://gitlab.com/sparqhub/edge/commit/a5c1c2c))


### Features

* **CategoryPicker:** added component & docs ([417d242](https://gitlab.com/sparqhub/edge/commit/417d242))
* **Modal:** add new Modal component ([af04368](https://gitlab.com/sparqhub/edge/commit/af04368))

## [1.5.8](https://gitlab.com/sparqhub/edge/compare/v1.5.7...v1.5.8) (2019-08-24)


### Bug Fixes

* remove duplicate props ([0d59ed9](https://gitlab.com/sparqhub/edge/commit/0d59ed9))

## [1.5.7](https://gitlab.com/sparqhub/edge/compare/v1.5.6...v1.5.7) (2019-08-24)


### Bug Fixes

* conventinal import/export ([2af654a](https://gitlab.com/sparqhub/edge/commit/2af654a))
* **Box:** remove 'm' and 'p' options in favor of separate 'y' and 'x' values, rename spacing helper functions ([4c9d443](https://gitlab.com/sparqhub/edge/commit/4c9d443))
* few fixes and improvements, Option selected state ([e10e932](https://gitlab.com/sparqhub/edge/commit/e10e932))
* spacing changes in docs ([0fc3cfa](https://gitlab.com/sparqhub/edge/commit/0fc3cfa))

## [1.5.6](https://gitlab.com/sparqhub/edge/compare/v1.5.5...v1.5.6) (2019-08-21)


### Bug Fixes

* Icon color, Button border, body text color ([5421515](https://gitlab.com/sparqhub/edge/commit/5421515))

## [1.5.5](https://gitlab.com/sparqhub/edge/compare/v1.5.4...v1.5.5) (2019-08-20)


### Bug Fixes

* add body margin 0 ([2975665](https://gitlab.com/sparqhub/edge/commit/2975665))

## [1.5.4](https://gitlab.com/sparqhub/edge/compare/v1.5.3...v1.5.4) (2019-08-19)


### Bug Fixes

* more font weights ([5dee342](https://gitlab.com/sparqhub/edge/commit/5dee342))

## [1.5.3](https://gitlab.com/sparqhub/edge/compare/v1.5.2...v1.5.3) (2019-08-18)


### Bug Fixes

* **Link:** add disabled style ([77c25f7](https://gitlab.com/sparqhub/edge/commit/77c25f7))
* **Link:** link styles adjusted ([6e98197](https://gitlab.com/sparqhub/edge/commit/6e98197))
* **Option:** disabled font color ([ac6d4d1](https://gitlab.com/sparqhub/edge/commit/ac6d4d1))

## [1.5.2](https://gitlab.com/sparqhub/edge/compare/v1.5.1...v1.5.2) (2019-08-18)


### Bug Fixes

* export theme as partial ([c5e1347](https://gitlab.com/sparqhub/edge/commit/c5e1347))
* Link component, colors and CI fix ([83f1d3c](https://gitlab.com/sparqhub/edge/commit/83f1d3c))
* revamp colors, update deps ([13f5468](https://gitlab.com/sparqhub/edge/commit/13f5468))
* type-check script ([6276231](https://gitlab.com/sparqhub/edge/commit/6276231))

## [1.5.1](https://gitlab.com/sparqhub/edge/compare/v1.5.0...v1.5.1) (2019-07-31)


### Bug Fixes

* Select children validation ([bac2de2](https://gitlab.com/sparqhub/edge/commit/bac2de2))


### Performance Improvements

* update deps ([4ac9a34](https://gitlab.com/sparqhub/edge/commit/4ac9a34))

# [1.5.0](https://gitlab.com/sparqhub/edge/compare/v1.4.10...v1.5.0) (2019-07-21)


### Features

* export spacing and spacer utils ([5b208be](https://gitlab.com/sparqhub/edge/commit/5b208be))

## [1.4.10](https://gitlab.com/sparqhub/edge/compare/v1.4.9...v1.4.10) (2019-07-21)


### Bug Fixes

* large Button font size ([872fed8](https://gitlab.com/sparqhub/edge/commit/872fed8))

## [1.4.9](https://gitlab.com/sparqhub/edge/compare/v1.4.8...v1.4.9) (2019-07-21)


### Bug Fixes

* Input and Button borders, large Input inner Button style ([8a2a736](https://gitlab.com/sparqhub/edge/commit/8a2a736))

## [1.4.8](https://gitlab.com/sparqhub/edge/compare/v1.4.7...v1.4.8) (2019-07-21)


### Bug Fixes

* **Box:** flexFlow and justifyContent fixes ([378e38d](https://gitlab.com/sparqhub/edge/commit/378e38d))

## [1.4.7](https://gitlab.com/sparqhub/edge/compare/v1.4.6...v1.4.7) (2019-07-21)


### Bug Fixes

* screen property for Box component, cleanup ([baa0623](https://gitlab.com/sparqhub/edge/commit/baa0623))

## [1.4.6](https://gitlab.com/sparqhub/edge/compare/v1.4.5...v1.4.6) (2019-07-20)


### Bug Fixes

* disabled prop for Input and Select ([25f37cb](https://gitlab.com/sparqhub/edge/commit/25f37cb))
* Option right Icon position, Input type now can be passed straith to TextField, disabled Button cursor ([5680aba](https://gitlab.com/sparqhub/edge/commit/5680aba))

## [1.4.5](https://gitlab.com/sparqhub/edge/compare/v1.4.4...v1.4.5) (2019-07-20)


### Bug Fixes

* make default theme type available ([5038c09](https://gitlab.com/sparqhub/edge/commit/5038c09))

## [1.4.4](https://gitlab.com/sparqhub/edge/compare/v1.4.3...v1.4.4) (2019-07-20)


### Bug Fixes

* build types, flexFlow being required ([3d18bd2](https://gitlab.com/sparqhub/edge/commit/3d18bd2))

## [1.4.3](https://gitlab.com/sparqhub/edge/compare/v1.4.2...v1.4.3) (2019-07-20)


### Bug Fixes

* update dependencies ([dc1ca00](https://gitlab.com/sparqhub/edge/commit/dc1ca00))

## [1.4.2](https://gitlab.com/sparqhub/edge/compare/v1.4.1...v1.4.2) (2019-07-18)


### Bug Fixes

* emit declaration file ([73d7184](https://gitlab.com/sparqhub/edge/commit/73d7184))

## [1.4.1](https://gitlab.com/sparqhub/edge/compare/v1.4.0...v1.4.1) (2019-07-17)


### Bug Fixes

* **Button:** fixed variables, font-size, bg color ([7c5d1de](https://gitlab.com/sparqhub/edge/commit/7c5d1de))
* Size is not enum anymore, Box flexFlow property now can be added, Button example improved ([793716c](https://gitlab.com/sparqhub/edge/commit/793716c))
* **Input:** revert variant ([7a492d5](https://gitlab.com/sparqhub/edge/commit/7a492d5))

# [1.4.0](https://gitlab.com/sparqhub/edge/compare/v1.3.0...v1.4.0) (2019-07-15)


### Bug Fixes

* **Input:** margins fixed for different sizes ([3429d3a](https://gitlab.com/sparqhub/edge/commit/3429d3a))
* **Select:** fix open/close onClick behaviour ([35336b2](https://gitlab.com/sparqhub/edge/commit/35336b2))
* **TextField:** fixed margins for inner Icons and Buttons ([500a655](https://gitlab.com/sparqhub/edge/commit/500a655))
* build, add nvmrc ([90232ba](https://gitlab.com/sparqhub/edge/commit/90232ba))


### Features

* **Input:** size property ([b08d23e](https://gitlab.com/sparqhub/edge/commit/b08d23e))

# [1.3.0](https://gitlab.com/sparqhub/edge/compare/v1.2.0...v1.3.0) (2019-07-10)


### Bug Fixes

* dependencies cleanup ([71efc12](https://gitlab.com/sparqhub/edge/commit/71efc12))
* **Option:** fix Icon styles ([3d61870](https://gitlab.com/sparqhub/edge/commit/3d61870))
* **Select:** cleaned up, simplified ([ea81335](https://gitlab.com/sparqhub/edge/commit/ea81335))


### Features

* **Dropdown:** autoClose prop, examples of usage ([e853279](https://gitlab.com/sparqhub/edge/commit/e853279))
* **Select:** add types, examples, don't render wrapper div ([3954791](https://gitlab.com/sparqhub/edge/commit/3954791))
* **Select:** added popper.js for dropdown placement ([2281cdd](https://gitlab.com/sparqhub/edge/commit/2281cdd))
* **Select:** new Select options ([5251d5b](https://gitlab.com/sparqhub/edge/commit/5251d5b))
* **Select:** Select is now usable, supports keyboard navigation ([cd747a7](https://gitlab.com/sparqhub/edge/commit/cd747a7))

# [1.2.0](https://gitlab.com/sparqhub/edge/compare/v1.1.0...v1.2.0) (2019-07-05)


### Bug Fixes

* **margin:** small margin fixes ([ce23e7c](https://gitlab.com/sparqhub/edge/commit/ce23e7c))


### Features

* **Select:** added select component ([371b49d](https://gitlab.com/sparqhub/edge/commit/371b49d))

# Changelog for Edge lib
