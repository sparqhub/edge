# @sparqhub/edge

[![pipeline status](https://gitlab.com/sparqhub/edge/badges/master/pipeline.svg)](https://gitlab.com/sparqhub/edge/commits/master)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
![npm (scoped)](https://img.shields.io/npm/v/@sparqhub/edge.svg?style=flat-square)

Component library for SparqHub

# [Documentation](https://sparqhub.gitlab.io/edge/)
