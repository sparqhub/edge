import styled, { css } from 'styled-components';
import { padding, margin } from 'utils';
import { Margin, Padding, Size } from 'types/helpers';

export type Props = Padding &
  Margin & {
    /** Quick short cuts for the most popular values */
    display?: 'block' | 'inline' | 'inline-block' | 'flex' | 'inline-flex';
    /** See [`flex`](https://developer.mozilla.org/en-US/docs/Web/CSS/flex) property */
    flex?: number | string;
    /** Restricts content size by breakpoints, centering restricted content on the screen. Uses `theme.core.screen` */
    screen?: Size;
    justifyContent?:
      | 'flex-start'
      | 'flex-end'
      | 'space-between'
      | 'space-around'
      | 'space-evenly'
      | 'stretch'
      | 'center'
      | 'unset';
    alignItems?: 'flex-start' | 'flex-end' | 'center' | 'baseline' | 'stretch';
    alignContent?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around' | 'stretch';
    flexDirection?: 'row' | 'column' | 'row-reverse' | 'column-reverse';
    flexWrap?: 'wrap' | 'nowrap' | 'wrap-reverse';
    flexFlow?:
      | 'row wrap'
      | 'row nowrap'
      | 'row wrap-reverse'
      | 'column wrap'
      | 'column nowrap'
      | 'column wrap-reverse'
      | 'row-reverse wrap'
      | 'row-reverse nowrap'
      | 'row-reverse wrap-reverse'
      | 'column-reverse wrap'
      | 'column-reverse nowrap'
      | 'column-reverse wrap-reverse';
  };

const styles = css<Props>`
  ${({ display, flex, justifyContent, alignItems, alignContent, flexDirection, flexWrap, flexFlow }) => css`
    display: ${display};
    flex: ${flex};
    justify-content: ${justifyContent};
    align-items: ${alignItems};
    align-content: ${alignContent};
    flex-direction: ${flexDirection};
    flex-wrap: ${flexWrap};
    flex-flow: ${flexFlow};
  `}
  ${({ screen, theme }) =>
    screen &&
    css`
      margin-left: auto;
      margin-right: auto;
      width: 100%;
      max-width: ${theme.screen[screen]};
    `}
`;

export const Box = styled.div<Props>`
  ${padding}
  ${margin}
  ${styles}
`;

Box.defaultProps = {
  display: 'block',
  mx: 0,
  my: 0,
  px: 0,
  py: 0
};

export default Box;
