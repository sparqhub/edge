Boxes can be used to align your content. Borders are added for visibility:

```jsx
const withBorder = { border: '1px solid #e8e8e8' };

<>
  <Box style={withBorder}>Default box</Box>
  <Box px={0.5} py={2} mx={8} my={3} style={withBorder}>
    Applies spacings
  </Box>
</>;
```

It is also align box content using [flexbox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox) positioning:

```jsx
<Box display="flex" justifyContent="space-between">
  <div>Hello world!</div>
  <div>I am flexbox now!</div>
</Box>
```

And nest into structures:

```jsx
import { Icon } from '@sparqhub/edge';

const style = {
  backgroundColor: '#F7F7F7',
  width: '200px'
};
const iconStyle = { marginRight: '8px' };

const items = [{ icon: 'refresh', text: 'Refresh' }, { icon: 'star', text: 'Make magic' }];

<Box display="flex" flexDirection="column" alignItems="flex-start">
  {items.map(({ icon, text }) => (
    <Box display="flex" inline alignItems="center" py={1} px={1} style={style} key={text}>
      <Box style={iconStyle}>
        <Icon icon={icon} size="s" />
      </Box>
      {text}
    </Box>
  ))}
</Box>;
```

Restrict content size using `screen` property:

```jsx
const style = {
  backgroundColor: '#F1F1F1',
  height: '100px'
};
<>
  <Box screen="xs" display="flex" justifyContent="center" alignItems="center" style={style}>
    Size xs (Mobile)
  </Box>
  <Box screen="s" display="flex" justifyContent="center" alignItems="center" style={style}>
    Size s (Tablet)
  </Box>
  <Box screen="m" display="flex" justifyContent="center" alignItems="center" style={style}>
    Size m (Normal)
  </Box>
</>;
```
