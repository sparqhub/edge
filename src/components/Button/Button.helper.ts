import styled, { css } from 'styled-components';

import Icon from 'components/Icon';

import { makeSpacing, makeSpace, borderRadius } from 'utils';
import { Size } from 'types/helpers';

const sizeMap = {
  xs: css`
    font-size: 10px;
    height: 20px;
    padding: ${makeSpacing(0, 1)};
  `,
  s: css`
    font-size: 12px;
    height: 26px;
    padding: ${makeSpacing(0, 1.25)};
  `,
  m: css`
    font-size: 13px;
    height: 32px;
    padding: ${makeSpacing(0, 2)};
  `,
  l: css`
    font-size: 16px;
    height: 40px;
    padding: ${makeSpacing(0, 2)};
  `
};

const darkText = {
  default: true,
  transparent: true,
  flat: true
};
const hoverBorder = {
  default: true
};

export interface StyledProps {
  href?: string;
  size?: Exclude<Size, 'xl'>;
  variant?: 'default' | 'flat' | 'transparent' | 'primary' | 'warning' | 'danger';
}

const StyledButton = styled.button<StyledProps>`
  --bg-color: ${({ theme, variant }) => theme.colors.button[variant].default};
  --border-color: ${({ theme, variant }) => theme.colors.common.border[variant]};
  background-color: var(--bg-color, transparent);
  border: 1px solid var(--border-color, transparent);
  color: ${({ theme, variant }) => (darkText[variant] ? theme.colors.gray[900] : '#fff')};
  transition: background-color 0.1s, border-color 0.1s;
  cursor: pointer;
  text-decoration: none;
  display: inline-flex;
  align-items: center;
  flex-flow: row nowrap;
  flex-shrink: 0;
  box-sizing: border-box;
  ${borderRadius}
  ${({ size }) => sizeMap[size]}
  &:hover {
    --bg-color: ${({ theme, variant }) => theme.colors.button[variant].hover};
  }
  &:active,
  &:hover {
    --border-color: ${({ theme, variant }) => hoverBorder[variant] && theme.colors.common.border.hover};
  }
  &:active {
    --bg-color: ${({ theme, variant }) => theme.colors.button[variant].active};
  }
  &:focus {
    outline: none;
    --border-color: ${({ theme }) => theme.colors.gray[700]};
  }
  &:disabled {
    position: relative;
    --border-color: transparent;
    opacity: 0.8;
    color: ${({ theme, variant }) => (darkText[variant] ? theme.colors.gray[600] : theme.colors.gray[200])};
    pointer-events: none;
    cursor: not-allowed;
    &:after {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      cursor: not-allowed;
      pointer-events: all;
    }
  }
  & > ${Icon} {
    font-size: 1.25em;
    line-height: 1;
  }
  & > ${Icon}:first-child:not(:last-child) {
    margin-right: ${makeSpace(1)};
  }
  & > ${Icon}:last-child:not(:first-child) {
    margin-left: ${makeSpace(1)};
  }
`;

export default StyledButton;
