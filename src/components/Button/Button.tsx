import React, { isValidElement } from 'react';
import StyledButton from './Button.helper';
import Icon from 'components/Icon';

type Props = React.ComponentProps<typeof StyledButton>;

export const Button = React.forwardRef<HTMLButtonElement, Props>(({ href, children, ...props }, ref) => {
  const tag = href ? 'a' : 'button';

  return (
    <StyledButton as={tag} href={href} {...props} ref={ref}>
      {React.Children.map(children, child => {
        if (!isValidElement(child)) {
          // wrap string/number children in <span /> for positioning
          return <span>{child}</span>;
        }
        if (child.type === Icon) {
          return React.cloneElement(child, {
            size: props.size
          });
        }
        return child;
      })}
    </StyledButton>
  );
});

Button.defaultProps = {
  size: 'm',
  variant: 'default',
  role: 'button'
};
Button.displayName = 'Button';

/** @component */
export default Button;
