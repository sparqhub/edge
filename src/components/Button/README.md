Button component

```jsx
import { Box } from '@sparqhub/edge';

const withMargin = {
  margin: '8px'
};

<Box display="flex" flexDirection="column">
  <Box display="flex" alignItems="center" justifyContent="space-evenly">
    <Button style={withMargin}>Default</Button>
    <Button variant="transparent" style={withMargin}>
      Transparent
    </Button>
    <Button variant="flat" style={withMargin}>
      Flat
    </Button>
    <Button variant="primary" style={withMargin}>
      Primary
    </Button>
    <Button variant="warning" style={withMargin}>
      Warning
    </Button>
    <Button variant="danger" style={withMargin}>
      Danger
    </Button>
  </Box>
  <Box display="flex" alignItems="center" justifyContent="space-evenly">
    <Button disabled style={withMargin}>
      Default
    </Button>
    <Button disabled variant="transparent" style={withMargin}>
      Transparent
    </Button>
    <Button disabled variant="flat" style={withMargin}>
      Flat
    </Button>
    <Button disabled variant="primary" style={withMargin}>
      Primary
    </Button>
    <Button disabled variant="warning" style={withMargin}>
      Warning
    </Button>
    <Button disabled variant="danger" style={withMargin}>
      Danger
    </Button>
  </Box>
</Box>;
```

Also styles Icon:

```jsx
import { Box, Icon } from '@sparqhub/edge';

<Box display="flex" justifyContent="space-around" style={{ height: '200px' }}>
  <Box display="flex" flexFlow="column nowrap" justifyContent="space-around" alignItems="center">
    <Button size="xs">
      <Icon icon="more_horiz" />
    </Button>
    <Button size="s">
      <Icon icon="more_horiz" />
    </Button>
    <Button>
      <Icon icon="more_horiz" />
    </Button>
    <Button size="l">
      <Icon icon="more_horiz" />
    </Button>
  </Box>
  <Box display="flex" flexFlow="column nowrap" justifyContent="space-around" alignItems="center">
    <Button variant="flat" size="xs">
      <Icon icon="more_horiz" />
    </Button>
    <Button variant="flat" size="s">
      <Icon icon="more_horiz" />
    </Button>
    <Button variant="flat">
      <Icon icon="more_horiz" />
    </Button>
    <Button variant="flat" size="l">
      <Icon icon="more_horiz" />
    </Button>
  </Box>
  <Box display="flex" flexFlow="column nowrap" justifyContent="space-around" alignItems="center">
    <Button variant="transparent" size="xs">
      <Icon icon="sync" />
      Synchronization
    </Button>
    <Button variant="transparent" size="s">
      <Icon icon="sync" />
      Synchronization
    </Button>
    <Button variant="transparent">
      <Icon icon="sync" />
      Synchronization
    </Button>
    <Button variant="transparent" size="l">
      <Icon icon="sync" />
      Synchronization
      <Icon icon="more_vert" />
    </Button>
  </Box>
  <Box display="flex" flexFlow="column nowrap" justifyContent="space-around" alignItems="center">
    <Button variant="warning" size="xs">
      <Icon icon="warning" />
      Warning
    </Button>
    <Button variant="warning" size="s">
      <Icon icon="warning" />
      Warning
    </Button>
    <Button variant="warning">
      <Icon icon="warning" />
      Warning
    </Button>
    <Button variant="warning" size="l">
      <Icon icon="warning" />
      Warning
    </Button>
  </Box>
  <Box display="flex" flexFlow="column nowrap" justifyContent="space-around" alignItems="center">
    <Button variant="danger" size="xs">
      <Icon icon="error" />
      Error
    </Button>
    <Button variant="danger" size="s">
      <Icon icon="error" />
      Error
    </Button>
    <Button variant="danger">
      <Icon icon="error" />
      Error
    </Button>
    <Button variant="danger" size="l">
      <Icon icon="error" />
      Error
    </Button>
  </Box>
</Box>;
```
