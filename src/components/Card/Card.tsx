import styled from 'styled-components';
import { elevation, borderRadius, makeSpace } from 'utils';
import { Elevation } from 'types/helpers';
import Box from 'components/Box';

interface Props {
  elevation?: Elevation;
}

export const Card = styled(Box)<Props>`
  background-color: ${({ theme }) => theme.colors.common.card.default};
  margin-bottom: ${({ my }) => !my && makeSpace(1)};
  ${borderRadius}
  ${elevation};
`;

Card.defaultProps = {
  elevation: 2,
  px: 2,
  py: 2
};

export default Card;
