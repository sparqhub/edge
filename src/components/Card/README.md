Elevation:

```jsx
<Card elevation={0}>Elevation 0</Card>
<Card elevation={1}>Elevation 1</Card>
<Card elevation={2}>Elevation 2 (default)</Card>
<Card elevation={3}>Elevation 3</Card>
<Card elevation={4}>Elevation 4</Card>
<Card elevation={8} my={4}>Elevation 8</Card>
<Card elevation={16} my={4}>Elevation 16</Card>
<Card elevation={24} px={4} my={6}>Elevation 24</Card>
```

Component also takes same properties as [`Box`](#box) component for spacings. Those are defaults:

```jsx
<Card>
  Default padding (px = 2, py = 2) and margin bottom 1 `spacing` value. See <a href="#box">Box</a> component for
  details.
</Card>
```
