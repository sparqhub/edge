import React from 'react';

import Option from 'components/Option';
import { validateOption } from 'components/Option/utils';

import { Container } from './Picker.helper';

type Props<T = any> = {
  value: T;
  onChange: (newValue: T) => any;
  children: React.ReactNodeArray;
};

export const CategoryPicker: React.FC<Props> = ({ value, onChange, children, ...props }) => {
  return (
    <Container display="flex" {...props}>
      {React.Children.map(children, (child: React.ReactElement<React.ComponentProps<typeof Option>>) => {
        const onClick = e => {
          onChange(child.props.value);

          if (child.props.onClick) {
            child.props.onClick(e);
          }
        };

        const onEnterPress = e => {
          if (e.charCode === 13) {
            onChange(child.props.value);
          }
          if (child.props.onKeyPressCapture) {
            child.props.onKeyPressCapture(e);
          }
        };
        return React.cloneElement(child, {
          onClick,
          selected: value === child.props.value,
          bordered: true,
          tabIndex: 0,
          onKeyPressCapture: onEnterPress
        });
      })}
    </Container>
  );
};

CategoryPicker.propTypes = {
  children: validateOption
};

export default CategoryPicker;
