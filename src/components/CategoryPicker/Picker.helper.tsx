import { OptionStyled } from 'components/Option/Option.helper';
import styled from 'styled-components';
import Box from 'components/Box';
import { borderRadius } from 'utils';

export const Container = styled(Box)`
box-sizing: border-box;
background-color: ${({ theme }) => theme.colors.gray[100]};
border: 1px solid ${({ theme }) => theme.colors.gray[300]};
${borderRadius}

& > ${OptionStyled} {
  margin-top: -1px;
  margin-bottom: -1px;
  box-sizing: border-box;
  flex: 1;
  justify-content: center;
}
& ${OptionStyled} + ${OptionStyled}:not(:last-child) {
  border-radius: 0;
}
& ${OptionStyled}:first-child {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}
& ${OptionStyled}:last-child {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
}
`;
