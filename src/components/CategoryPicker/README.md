Category picker is used as an alternative for radio buttons:

```jsx
import React, { useState } from 'react';
import { Option } from '@sparqhub/edge';

const Component = () => {
  const [value, setValue] = useState('Pastry');

  return (
    <CategoryPicker value={value} onChange={setValue}>
      {['Meal', 'Pastry', 'Grocery', 'Bread', 'Drinks'].map(name => (
        <Option key={name} value={name}>
          {name}
        </Option>
      ))}
    </CategoryPicker>
  );
};

<Component />;
```
