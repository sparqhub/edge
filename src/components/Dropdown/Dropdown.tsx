import React, { useMemo, useState, ReactElement, Fragment, useEffect } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

import { usePopper, composeRefs } from './utils';
import { borderRadius, elevation } from 'utils';
import { Elevation } from 'types/helpers';
import { PopperOptions } from 'popper.js';

const MenuWrapper = styled.div`
  min-width: 120px;
  z-index: 2000;
`;

const Menu = styled.ul<{ elevation: Elevation }>`
  list-style: none;
  padding: 4px 0;
  background-color: #fff;
  margin: 0;
  max-height: 200px;
  overflow-y: auto;
  overflow-x: hidden;
  ${borderRadius}
  ${elevation}
`;

interface Props {
  content: ReactElement;
  isOpen?: boolean;
  elevation?: Elevation;
  autoClose?: boolean;
  popperOptions: PopperOptions;
  /** @ignore */
  children: React.ReactElement;
}

const Dropdown = React.forwardRef<HTMLElement, Props>(
  ({ elevation, isOpen, content, children: trigger, autoClose, popperOptions, ...props }, ref) => {
    const [localIsOpen, setOpen] = useState(false);
    const isDropdownOpen = isOpen !== undefined ? isOpen : localIsOpen;
    const [triggerRef, popperRef] = usePopper({ isOpen: isDropdownOpen, popperOptions });

    useEffect(() => {
      if (isOpen !== undefined || !autoClose) {
        return;
      }
      const outsideClickOpen = e => {
        if (!triggerRef.current || !popperRef.current) {
          return;
        }
        if (triggerRef.current.contains(e.target) || popperRef.current.contains(e.target)) {
          return;
        }
        setOpen(false);
      };
      document.addEventListener('click', outsideClickOpen);
      return () => {
        document.removeEventListener('click', outsideClickOpen);
      };
    }, []);

    const triggerWithRef = useMemo(() => {
      const props: Record<string, any> = {
        ref: composeRefs(triggerRef, (trigger as any).ref)
      };
      if (isOpen === undefined) {
        props.onClick = (e: React.MouseEvent<any, MouseEvent>) => {
          setOpen(state => !state);
          const { onClick } = trigger.props;
          if (onClick) {
            onClick(e);
          }
        };
      }

      return React.cloneElement(trigger, props);
    }, [trigger, isOpen === undefined]);

    const menuRef = composeRefs(popperRef, ref);

    return (
      <Fragment>
        {triggerWithRef}
        {content ? (
          ReactDOM.createPortal(
            <MenuWrapper {...props} ref={menuRef}>
              {isDropdownOpen && Boolean(content) ? <Menu elevation={elevation}>{content}</Menu> : null}
            </MenuWrapper>,
            document.body
          )
        ) : (
          <MenuWrapper {...props} ref={menuRef} />
        )}
      </Fragment>
    );
  }
);

export const DropdownWrapper = React.forwardRef<HTMLElement, Props>((props, ref) => {
  const { content, children } = props;
  if (!content) {
    return children;
  }

  return <Dropdown {...props} ref={ref} />;
});

DropdownWrapper.defaultProps = {
  elevation: 2,
  autoClose: true
};

DropdownWrapper.displayName = 'Dropdown';

export default DropdownWrapper;
