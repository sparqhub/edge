Allows to have dropdown menu under the trigger. Automatically closes dropdown on click outside.

```jsx
import { Box } from '@sparqhub/edge';

const borderStyle = {
  border: '1px solid #222'
};

<Dropdown
  content={
    <Box px={1} py={1}>
      This is content
    </Box>
  }
>
  <Box style={borderStyle} px={1} py={1} display="inline">
    This is trigger
  </Box>
</Dropdown>;
```

Trigger can be any HTML element, but not a text node:

```jsx
import { Button } from '@sparqhub/edge';

<Dropdown content="Hello world">
  <Button>Click on me</Button>
</Dropdown>;
```

Elevation can be controlled:

```jsx
import { Button, Icon, Option } from '@sparqhub/edge';

<Dropdown
  elevation={24}
  content={
    <>
      <Option>
        <Icon icon="home" variant="outlined" />
        Home
      </Option>
      <Option>
        <Icon icon="view_week" variant="outlined" />
        Calendar
      </Option>
      <Option>
        <Icon icon="person" variant="outlined" />
        Profile
      </Option>
    </>
  }
>
  <Button variant="primary">Open menu</Button>
</Dropdown>;
```
