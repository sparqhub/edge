import { useRef, useLayoutEffect } from 'react';
import Popper, { PopperOptions } from 'popper.js';

interface Props {
  isOpen: boolean;
  popperOptions: PopperOptions;
}

export function usePopper({ isOpen, popperOptions = {} }: Props) {
  const triggerRef = useRef<HTMLElement>();
  const popperRef = useRef<HTMLUListElement>();
  const popperInstanceRef = useRef<Popper>(null);

  useLayoutEffect(() => {
    if (isOpen && popperRef.current) {
      const options: PopperOptions = {
        placement: 'bottom-start',
        ...popperOptions,
        modifiers: {
          ...popperOptions.modifiers,
          setWidth: {
            enabled: true,
            order: 800,
            fn: setWidthModifier
          }
        }
      };
      popperInstanceRef.current = new Popper(triggerRef.current, popperRef.current, options);
    }

    return () => {
      if (popperInstanceRef.current) {
        popperInstanceRef.current.destroy();
        popperInstanceRef.current = null;
      }
    };
  }, [isOpen]);

  useLayoutEffect(() => {
    if (popperInstanceRef.current) {
      popperInstanceRef.current.scheduleUpdate();
    }
  });

  return [triggerRef, popperRef];
}

export function composeRefs(...refs: (((instance: HTMLElement) => void) | React.MutableRefObject<HTMLElement>)[]) {
  return (instance: HTMLElement) => {
    refs.forEach(ref => {
      if (typeof ref === 'function') {
        ref(instance);
      } else if (ref !== null && typeof ref === 'object') {
        ref.current = instance;
      }
    });
  };
}

function setWidthModifier(data: Parameters<Popper.ModifierFn>[0]) {
  const {
    offsets: { reference, popper },
    styles
  } = data;
  data.styles.width = `${reference.width > popper.width ? reference.width : styles.width}px`;
  return data;
}
