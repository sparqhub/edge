import React, { FC } from 'react';
import styled, { css } from 'styled-components';
import cx from 'classnames';

import { Size } from 'types/helpers';

interface Props {
  icon: string;
  variant?: 'default' | 'outlined' | 'round' | 'sharp' | 'two-tone';
  size?: Size;
  /** @ignore */
  className?: string;
}

const Icon: FC<Props> = ({ icon, variant, className, ...props }) => {
  return (
    <i className={cx('material-icons', variant !== 'default' && `material-icons-${variant}`, className)} {...props}>
      {icon}
    </i>
  );
};

const styleToFont: Record<Props['variant'], string> = {
  'default': 'Material Icons',
  'outlined': 'Material Icons Outlined',
  'round': 'Material Icons Round',
  'sharp': 'Material Icons Sharp',
  'two-tone': 'Material Icons Two Tone'
};

const sizes: Record<Size, number> = {
  xs: 14,
  s: 18,
  m: 24,
  l: 32,
  xl: 48
};

export const StyledIcon = styled(Icon)`
  /* -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  font-family: ${({ variant }) => styleToFont[variant]};
  font-style: normal;
  font-weight: normal;
  font-display: block;
  text-transform: none;
  letter-spacing: normal;
  word-wrap: normal;
  white-space: nowrap;
  direction: ltr;
  -webkit-font-smoothing: antialiased;
  text-rendering: optimizeLegibility;
  -moz-osx-font-smoothing: grayscale;
  font-feature-settings: 'liga'; */
  user-select: none;
  pointer-events: none;
  color: inherit;
  ${({ size }) => {
    const mapSize = sizes[size];
    return css`
      font-size: ${mapSize}px;
      line-height: ${mapSize}px;
    `;
  }};
  vertical-align: sub;
  flex-shrink: 0;
`;

StyledIcon.defaultProps = {
  size: 'm',
  variant: 'default'
};

StyledIcon.displayName = 'Icon';

/** @component */
export default StyledIcon;
