Icon component accepts accepts `icon` prop instead of children, because it's shorter.

You can find the list of icons there: [Material Icons](https://material.io/tools/icons/)

```jsx
import { Box, Icon } from '@sparqhub/edge';

<Box display="flex" justifyContent="space-evenly" alignItems="center">
  <Icon icon="face" size="xs" />
  <Icon icon="face" size="s" />
  <Icon icon="face" size="m" />
  <Icon icon="face" size="l" />
  <Icon icon="face" size="xl" />
</Box>;
```

Icons support `variant` property which sets style of the icon:

```jsx
import { Box, Icon } from '@sparqhub/edge';

<Box display="flex" justifyContent="space-evenly" alignItems="center" my={2}>
  <Icon icon="settings" size="l" />
  <Icon icon="settings" variant="sharp" size="l" />
  <Icon icon="settings" variant="round" size="l" />
  <Icon icon="settings" variant="outlined" size="l" />
  <Icon icon="settings" variant="two-tone" size="l" />
</Box>;
```
