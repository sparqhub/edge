import styled, { css } from 'styled-components';
import { makeSpacing } from 'utils';

type Props = { active?: boolean; disabled?: boolean };

export const Link = styled.a<Props>`
  color: ${({ active, theme }) => (active ? theme.colors.gray[900] : theme.colors.gray[500])};
  text-decoration: none;
  font-weight: 700;
  transition: color 0.12s;
  cursor: pointer;
  padding: ${makeSpacing(0.5, 1)};
  &:hover {
    color: ${({ active, theme }) => (active ? theme.colors.gray[900] : theme.colors.gray[700])};
  }
  ${({ disabled, theme }) =>
    disabled &&
    css`
      pointer-events: none;
      cursor: default;
      color: ${theme.colors.gray[400]};
    `}
`;

export default Link;
