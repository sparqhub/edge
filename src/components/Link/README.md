Simple link component

```jsx
import { Box, Link } from '@sparqhub/edge';

<Box display="flex" justifyContent="space-evenly" alignItems="center" screen="m">
  <Link href="#link" active>
    Home
  </Link>
  <Link href="#link">Contacts</Link>
  <Link href="#link">About</Link>
  <Link href="#link" disabled>
    Unavailable
  </Link>
</Box>;
```
