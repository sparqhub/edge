import React, { useState, useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';

import ModalWindow from './ModalWindow.helper';

type Props = {
  trigger: React.ReactElement;
  onClose?: () => any;
};

export const Modal: React.FC<Props> = ({ trigger, onClose, children, ...props }) => {
  const [isOpen, changeOpen] = useState(false);

  const TriggerElement = useMemo(() => {
    const onClick = e => {
      changeOpen(state => !state);

      if (trigger.props.onClick) {
        trigger.props.onClick(e);
      }
    };
    return React.cloneElement(trigger, { onClick });
  }, [trigger]);

  const closeHandler = () => {
    changeOpen(false);
    onClose && onClose();
  };

  return (
    <>
      {TriggerElement}
      {isOpen && (
        <ModalWindow onClose={closeHandler} {...props}>
          {children}
        </ModalWindow>
      )}
    </>
  );
};

Modal.propTypes = {
  trigger: PropTypes.element.isRequired
};

export default Modal;
