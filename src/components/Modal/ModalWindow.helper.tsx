import React, { useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

import Button from 'components/Button';
import Card from 'components/Card';
import Icon from 'components/Icon';

import { makeSpace } from 'utils';

const Backdrop = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.2);
  overflow-y: auto;
  z-index: 1000;
`;

const ModalCard = styled(Card)`
  max-width: 600px;
  margin: 10vh auto;
  position: relative;
`;

const CloseButton = styled(Button)`
  position: absolute;
  top: ${makeSpace(1)};
  right: ${makeSpace(1)};
  padding: 0;
  justify-content: center;
  width: 34px;
  height: 34px;
`;

type Props = { onClose: () => any };

const ModalWindow: React.FC<Props> = ({ onClose, children, ...props }) => {
  useEscapeHandler(onClose);

  const onBackdropClick = e => {
    if (e.target === e.currentTarget) {
      onClose();
    }
  };

  return ReactDOM.createPortal(
    <Backdrop onClick={onBackdropClick} role="dialog" {...props}>
      <ModalCard elevation={4}>
        <CloseButton variant="transparent" onClick={onClose}>
          <Icon icon="close" />
        </CloseButton>
        {children}
      </ModalCard>
    </Backdrop>,
    document.body
  );
};

export default ModalWindow;

function useEscapeHandler(onClose) {
  const cbRef = useRef<() => any>(onClose);

  useEffect(() => {
    const handler = e => {
      if (e.keyCode === 27) {
        e.preventDefault();
        cbRef.current();
      }
    };
    document.addEventListener('keydown', handler, false);

    return () => {
      document.removeEventListener('keydown', handler);
    };
  }, []);
}
