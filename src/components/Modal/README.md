Modal can accept any component which represents DOM node with possible onClick handler:

```jsx
import { Button } from '@sparqhub/edge';

<Modal trigger={<Button>Open modal</Button>}>Some content</Modal>;
```

At the same time, trigger can have own `onClick` handler which gets executed after modal open. In the next example, alert is shown before the modal because of the blocking behaviour of `alert`: `Modal`'s handler set opened state to `true`, but the execution React rerender is paused by `alert`:

```jsx
import { Button } from '@sparqhub/edge';

<Modal
  trigger={
    <Button
      data-text="Hello world!"
      onClick={e => window.alert(`This is target dataset text: ${e.currentTarget.dataset.text}`)}
    >
      Open modal
    </Button>
  }
>
  Some content
</Modal>;
```

Scrollable content example:

```jsx
import { Button } from '@sparqhub/edge';

<Modal trigger={<Button>Open modal</Button>}>
  <div style={{ height: '120vh', textAlign: 'center' }}>
    <h1>This is an example of some long modal</h1>
    <p>Hello world!</p>
  </div>
</Modal>;
```
