import styled, { css } from 'styled-components';

import { makeSpace, borderRadius, makeSpacing } from 'utils';
import Icon from 'components/Icon';

export interface Props<V = any> {
  /** `value` of the Option in Select component */
  value?: V;
  /** Shown label for the Option
   *  (picked by Select component to show selected value text) */
  itemLabel?: string;
  /** Disables the option */
  disabled: boolean;
  /** Applies selected styles */
  selected: boolean;
  /** @ignore */
  bordered: boolean;
  /** @ignore */
  active?: boolean;
}

const backgroundColor = ({ theme, active, selected }) =>
  selected ? theme.colors.gray[300] : active ? theme.colors.gray[200] : 'transparent';

export const OptionStyled = styled.div<Partial<Props>>`
  --border-color: transparent;
  --border-width: 0px;
  --bg-color: ${backgroundColor};
  padding: ${makeSpacing(1, 2)};
  ${borderRadius}
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  background-color: var(--bg-color);
  border: var(--border-width) solid var(--border-color);
  transition: border-color 0.12s, background-color 0.12s;
  &:focus {
    --border-color: ${({ theme }) => theme.colors.gray[700]};
    --bg-color: transparent;
    outline: none;
  }
  &:hover {
    --bg-color: ${({ theme, selected }) => backgroundColor({ theme, selected, active: true })};
  }
  ${({ bordered, selected, theme }) =>
    bordered &&
    css`
      --border-width: 1px;
      --border-color: ${selected ? theme.colors.gray[700] : 'transparent'};
      --bg-color: ${selected ? '#fff' : 'transparent'};
      box-sizing: border-box;
      &:hover {
        --bg-color: ${selected ? '#fff' : backgroundColor({ theme, selected: false, active: true })};
      }
    `}
  ${({ disabled }) =>
    disabled &&
    css`
      --bg-color: ${({ theme }) => theme.colors.gray[200]};
      pointer-events: none;
      color: ${({ theme }) => theme.colors.gray[600]};
      opacity: 0.6;
    `}
  & > ${Icon} {
    font-size: 1.25em;
    &:first-child {
      margin-left: 0;
      margin-right: ${makeSpace(2)};
    }
    &:last-child {
      margin-right: 0;
      margin-left: auto;
      padding-left: ${makeSpace(2)};
    }
  }
`;
