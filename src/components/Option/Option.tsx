import React, { isValidElement } from 'react';
import { OptionStyled } from './Option.helper';

type OptionProps = React.ComponentPropsWithRef<typeof OptionStyled>;

export const Option = React.forwardRef<any, OptionProps>(({ children, ...props }, ref: any) => {
  return (
    <OptionStyled {...props} ref={ref}>
      {React.Children.map(children, child => (isValidElement(child) ? child : <span>{child}</span>))}
    </OptionStyled>
  );
});

Option.displayName = 'Option';

export default Option;
