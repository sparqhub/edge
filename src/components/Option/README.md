`Option` element can be used in `Dropdown`s or `Select`s to be a selectable item.

```jsx
import { Box, Icon } from '@sparqhub/edge';

<Box display="inline-block">
  <Option>Default</Option>
  <Option disabled>Disabled</Option>
  <Option active>Active</Option>
  <Option selected>Selected</Option>
  <Option bordered selected>
    Selected with border
  </Option>

  <Option>
    <Icon icon="settings" mode="outlined" />
    With left icon
  </Option>
  <Option>
    With right icon
    <Icon icon="settings" mode="outlined" />
  </Option>
  <Option>
    <Icon icon="settings" mode="outlined" />
    With icons from both sides
    <Icon icon="keyboard_arrow_right" mode="outlined" />
  </Option>
</Box>;
```
