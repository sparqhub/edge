import React from 'react';
import Option from 'components/Option';

export function validateOption<T = object>(props: T, propName: string, componentName: string) {
  const isValidChildren = React.Children.toArray(props[propName]).every(
    child => React.isValidElement(child) && child.type === Option
  );
  if (!isValidChildren) {
    return new Error(`${componentName} only accepts "<Option />" elements`);
  }
}
