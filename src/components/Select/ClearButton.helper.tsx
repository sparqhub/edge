import React, { FC } from 'react';
import Button from 'components/Button';
import Icon from 'components/Icon';

import { useDownshiftOptions } from './utils';
import styled, { css } from 'styled-components';

const hiddenStyles = css`
  opacity: 0;
  pointer-events: none;
`;

const StyledButton = styled(Button)<{ isHidden: boolean }>`
  ${({ isHidden }) => isHidden && hiddenStyles}
`;

const ClearButton: FC = () => {
  const { selectedItem, clearSelection } = useDownshiftOptions();

  return (
    <StyledButton isHidden={!selectedItem} onClick={() => clearSelection()} tabIndex={-1} size="s">
      <Icon icon="clear" />
    </StyledButton>
  );
};

export default ClearButton;
