import React, { ReactNodeArray, FC } from 'react';

import Dropdown from 'components/Dropdown';
import { useFilteredItems, useDownshiftOptions } from './utils';
import { validateOption } from 'components/Option/utils';
import styled from 'styled-components';
import { OptionStyled } from 'components/Option/Option.helper';

interface Props {
  content: ReactNodeArray;
  filterable: boolean;
}

const DropdownStyled = styled(Dropdown)`
  & ${OptionStyled} + ${OptionStyled} {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
  & ${OptionStyled} + ${OptionStyled}:not(:last-child),
  & ${OptionStyled}:first-child {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
  }
`;

const SelectDropdown: FC<Props> = ({ content, filterable, children, ...props }) => {
  const { getMenuProps, isOpen } = useDownshiftOptions();
  const filtered = useFilteredItems(content, filterable);

  return (
    <DropdownStyled isOpen={isOpen} content={filtered} {...getMenuProps()} {...props}>
      {children}
    </DropdownStyled>
  );
};

SelectDropdown.propTypes = {
  content: validateOption
};

export default SelectDropdown;
