Select component with simple filter functionallity.

Simple select with text `Option`s:

```jsx
import { Option } from '@sparqhub/edge';

<Select value="Item 2" placeholder="String items" style={{ width: '150px' }}>
  {['Item 1', 'Item 2', 'Item 3', 'Item 4'].map(item => (
    <Option value={item} key={item}>
      {item}
    </Option>
  ))}
</Select>;
```

Disabled `Select`

```jsx
<Select disabled placeholder="I am disabled :(" />
```

Select with value/label objects and clearable enabled:

```jsx
import React, { useState } from 'react';

import { Box, Option, Icon } from '@sparqhub/edge';

const icons = [
  {
    title: 'Accessibility',
    icon: 'accessibility_new'
  },
  {
    title: 'Accessible forward',
    icon: 'accessible_forward'
  },
  {
    title: 'Accessible',
    icon: 'accessible'
  },
  {
    title: 'Bug report',
    icon: 'bug_report'
  }
];

const SelectWithState = () => {
  const [value, setState] = useState(icons[3].icon);

  return (
    <Box display="flex" alignItems="center">
      <Select value={value} onChange={setState} placeholder="With default value" clearable>
        {icons.map(item => (
          <Option value={item.icon} itemLabel={item.title} key={item.icon}>
            <Icon icon={item.icon} />
            {item.title}
          </Option>
        ))}
      </Select>
      <Box mx={3}>Selected value: {String(value)}</Box>
    </Box>
  );
};

<SelectWithState />;
```

Same with filtering and memoized Options:

```jsx
import React, { useState, useMemo } from 'react';

import { Box, Option, Icon } from '@sparqhub/edge';

const icons = [
  {
    title: 'Accessibility',
    icon: 'accessibility_new'
  },
  {
    title: 'Accessible forward',
    icon: 'accessible_forward'
  },
  {
    title: 'Accessible',
    icon: 'accessible'
  },
  {
    title: 'Bug report',
    icon: 'bug_report'
  }
];

const SelectWithState = ({ children, ...props }) => {
  const [value, setState] = useState(props.value || null);

  const options = useMemo(
    () =>
      icons.map(item => (
        <Option value={item.icon} itemLabel={item.title} key={item.icon}>
          <Icon icon={item.icon} />
          {item.title}
        </Option>
      )),
    []
  );

  return (
    <Box display="flex" alignItems="center">
      <Select
        onChange={value => {
          setState(value);
        }}
        placeholder="Search for an icon"
        filterable
        clearable
      >
        {options}
      </Select>
      <Box mx={3}>Selected value: {String(value)}</Box>
    </Box>
  );
};

<SelectWithState />;
```
