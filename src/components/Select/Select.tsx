import React, { FC, ReactNodeArray } from 'react';
import Downshift from 'downshift';

import { useDownshift, DownshiftContext } from './utils';
import Dropdown from './Dropdown.helper';
import Input from 'components/TextField';
import SelectInput from './SelectInput.helper';

interface Props<V = any> {
  /** Bound value to Select component */
  value: V;
  /** On value change event */
  onChange?: (value: V) => void;
  /** Placeholder text to shown when none is selected */
  placeholder?: string;
  /** Disables Select component */
  disabled?: boolean;
  /** Only `Option` component is allowed */
  children?: ReactNodeArray;
  /** Enables Input field for filtering */
  filterable?: boolean;
  /** Enables clear selection button */
  clearable?: boolean;
  /** Props to the Input component which is used for options filtering */
  inputProps?: React.ComponentPropsWithRef<typeof Input>;
}

export const Select: FC<Props> = ({
  children,
  value,
  onChange,
  placeholder,
  filterable,
  clearable,
  inputProps,
  ...props
}) => {
  const downshiftProps = useDownshift<typeof value>({ children, value, onChange });

  return (
    <Downshift suppressRefError {...downshiftProps}>
      {options => (
        <DownshiftContext.Provider value={options}>
          <Dropdown content={children} filterable={filterable}>
            <SelectInput
              filterable={filterable}
              placeholder={placeholder}
              inputProps={inputProps}
              clearable={clearable}
              {...options.getRootProps()}
              {...props}
            />
          </Dropdown>
        </DownshiftContext.Provider>
      )}
    </Downshift>
  );
};

/** @component */
export default Select;
