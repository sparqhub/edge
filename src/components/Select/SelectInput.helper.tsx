import React, { ComponentPropsWithRef } from 'react';

import TextField from 'components/TextField';
import Button from 'components/Button';
import Icon from 'components/Icon';
import { useDownshiftOptions } from './utils';
import ClearButton from './ClearButton.helper';

interface Props {
  placeholder: string;
  disabled?: boolean;
  filterable?: boolean;
  clearable?: boolean;
  inputProps: ComponentPropsWithRef<typeof TextField>['inputProps'];
}

const SelectInput = React.forwardRef<any, Props>(({ children, filterable, clearable, inputProps, ...props }, ref) => {
  const { getInputProps, selectedItem, openMenu, clearSelection, isOpen, getToggleButtonProps } = useDownshiftOptions();
  const { onChange: onChangeDownshift, value, ...downshiftProps } = getInputProps();
  const onChange = e => {
    if (selectedItem) {
      clearSelection();
    }
    onChangeDownshift(e);
    openMenu();
  };

  const toggleButtonProps = getToggleButtonProps();

  const toggleMenu = e => {
    if (!e.target.tagName.toLowerCase().includes('button') && e.currentTarget.contains(e.target)) {
      toggleButtonProps.onClick(e);
    }
  };

  const inputPropsMerged = Object.assign(downshiftProps, { readOnly: !filterable }, inputProps);

  return (
    <TextField
      onClick={toggleMenu}
      value={value || ''}
      onChange={onChange}
      {...props}
      inputProps={inputPropsMerged}
      ref={ref}
    >
      <Button {...toggleButtonProps} tabIndex="-1">
        <Icon icon={isOpen ? 'keyboard_arrow_up' : 'keyboard_arrow_down'} />
      </Button>
      {children}
      {clearable && <ClearButton />}
    </TextField>
  );
});

SelectInput.displayName = 'SelectInput';

export default SelectInput;
