import React, {
  useCallback,
  isValidElement,
  createContext,
  useContext,
  ReactText,
  useRef,
  useEffect,
  useLayoutEffect,
  ReactNodeArray
} from 'react';
import Popper from 'popper.js';
import Option from 'components/Option';
import { ControllerStateAndHelpers } from 'downshift';

export type OptionElement = React.ReactElement<React.ComponentProps<typeof Option>, typeof Option>;
export const DownshiftContext = createContext<ControllerStateAndHelpers<any>>(null);

interface Options<V> {
  value: V;
  onChange: (value: V) => void;
  children: ReactNodeArray;
}

export function useDownshift<V = any>({ value, onChange, children }: Options<V>) {
  const itemToString = useCallback(
    item => {
      let text;

      if (typeof item === 'string' || !item) {
        return item;
      }

      if (isValidElement(item) && item.type === Option) {
        const { itemLabel, children } = (item as OptionElement).props;
        text = itemLabel || (typeof children === 'string' && children);
      }
      if (typeof text !== 'string') {
        console.error(
          "Can't get text representation of your Option, consider adding `itemLabel` prop to Option components"
        );
      }
      return text;
    },
    [children]
  );

  const onStateChange = useCallback(
    ({ selectedItem }) => {
      if (!onChange) {
        return;
      }
      if (selectedItem === null) {
        onChange(null);
      } else if (selectedItem) {
        // clearing makes it null
        onChange((selectedItem as OptionElement).props.value);
      }
    },
    [onChange]
  );

  const selectedItem = value
    ? React.Children.toArray(children).find((item: OptionElement) => item.props.value === value)
    : null;

  return {
    onStateChange,
    selectedItem,
    itemToString
  };
}

export function useFilteredItems(content: ReactNodeArray = [], filter: boolean): ReactNodeArray {
  const { inputValue = '', selectedItem, itemToString, getItemProps, highlightedIndex } = useDownshiftOptions();

  const items =
    !filter || !inputValue || Boolean(selectedItem)
      ? content
      : React.Children.toArray(content).filter(child => {
          const label = itemToString(child);
          // keep selected item and text label matched
          return selectedItem === child || label.toLowerCase().includes(inputValue.toLowerCase());
        });

  return items.map((child: OptionElement, index) => {
    const isSelected = selectedItem && selectedItem.props.value === child.props.value;

    return React.cloneElement(child, {
      ...getItemProps({ item: child, index, isSelected }),
      active: highlightedIndex === index,
      selected: isSelected,
      as: 'li'
    });
  });
}

export function useDownshiftOptions() {
  return useContext(DownshiftContext);
}

export function usePopper(updatePopperDeps: any[]) {
  const { isOpen } = useDownshiftOptions();
  const ref = useRef<HTMLElement>();
  const popperRef = useRef<Popper>();

  useLayoutEffect(() => {
    if (!isOpen || !ref.current) {
      return;
    }
    popperRef.current = new Popper(ref.current.parentElement, ref.current);

    return () => {
      popperRef.current.destroy();
      popperRef.current = null;
    };
  }, [isOpen]);

  useEffect(() => {
    if (popperRef.current) {
      popperRef.current.scheduleUpdate();
    }
  }, updatePopperDeps);

  return ref;
}
