import styled from 'styled-components';

const Input = styled.input`
  all: unset;
  font-size: inherit;
  line-height: inherit;
  border: none;
  background: transparent;
  flex: 1 1 auto;
  order: 2;
  padding: 0;
  width: 100%;
  &:focus {
    outline: none;
  }
  & ~ * {
    order: 3;
  }
  & + * {
    order: 1;
  }
  ::-webkit-calendar-picker-indicator {
    display: none;
  }
  ::placeholder {
    color: ${({ theme }) => theme.colors.gray[500]};
  }
  &[readonly] {
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
  }
`;

export default Input;
