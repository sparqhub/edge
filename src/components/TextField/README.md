Usage examples:

```jsx
import { Box, Icon, Button } from '@sparqhub/edge';

const style = {
  marginBottom: '8px',
  width: '300px'
};

<>
  <Box display="flex" flexDirection="column" alignItems="flex-start">
    <TextField placeholder="Just input" style={style} />
    <TextField placeholder="Disabled input" disabled style={style} />
    <TextField placeholder="Input with Button inside" style={style}>
      <Button>
        <Icon icon="more_horiz" />
      </Button>
    </TextField>
    <TextField placeholder="Disabled with two Buttons inside" disabled style={style}>
      <Button>
        <Icon icon="search" />
      </Button>
      <Button color="primary">
        <Icon icon="bookmark_border" />
      </Button>
    </TextField>
    <TextField placeholder="Right Button only" style={style}>
      <div />
      <Button>
        <Icon icon="keyboard_arrow_down" />
      </Button>
    </TextField>
    <TextField placeholder="Icon only" style={style}>
      <Icon icon="search" />
    </TextField>
    <TextField placeholder="Icon and Button" style={style}>
      <Icon icon="search" />
      <Button>
        <Icon icon="keyboard_arrow_down" />
      </Button>
    </TextField>
    <TextField placeholder="Two Icons inside" style={style}>
      <Icon icon="search" />
      <Icon icon="keyboard_arrow_down" />
    </TextField>
  </Box>
</>;
```

`TextField` component also supports size property:

```jsx
import { Box, Icon, Button } from '@sparqhub/edge';

const style = {
  margin: '8px'
};

<Box display="flex" justifyContent="space-between" alignItems="center" flexWrap="wrap">
  <TextField placeholder="Small input" size="s">
    <Icon icon="search" />
    <Button>
      <Icon icon="keyboard_arrow_down" />
    </Button>
  </TextField>
  <TextField placeholder="Medium input (default)" size="m" style={style}>
    <Icon icon="search" />
    <Button>
      <Icon icon="keyboard_arrow_down" />
    </Button>
  </TextField>
  <TextField placeholder="Large input" size="l" style={style}>
    <Icon icon="search" />
    <Button variant="primary">Search</Button>
  </TextField>
</Box>;
```

Some unique cases can use `elevation` prop:

```jsx
import { Icon, Button } from '@sparqhub/edge';

<TextField placeholder="Search or type URL" elevation={3}>
  <Icon icon="search" />
  <Button>
    <Icon icon="bookmark_border" />
  </Button>
</TextField>;
```

Other types

```jsx
import { Box, Button, Icon } from '@sparqhub/edge';

<>
  <Box>
    <TextField type="date">
      <Icon icon="date_range" />
    </TextField>
  </Box>
  <Box style={{ marginTop: '16px' }}>
    <TextField placeholder={3000} type="number" inputProps={{ style: { width: '50px' }, min: 3000 }}>
      <span />
      <Button>
        <Icon icon="favorite" />
      </Button>
    </TextField>
  </Box>
</>;
```
