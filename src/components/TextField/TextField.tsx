import React, { ComponentPropsWithRef } from 'react';

import { Elevation, Size } from 'types/helpers';
import Wrapper from './Wrapper.helper';
import InputField from './Input.helper';

type InputProps = React.ComponentPropsWithRef<typeof InputField>;

type Props = ComponentPropsWithRef<typeof Wrapper> & {
  value?: InputProps['value'];
  onChange?: (ev: React.ChangeEvent<HTMLInputElement>) => void;
  placeholder?: InputProps['placeholder'];
  disabled?: InputProps['disabled'];
  type?: InputProps['type'];
  tabIndex?: InputProps['tabIndex'];
  inputProps?: InputProps;
};

export const TextField = React.forwardRef<HTMLDivElement, Props>(
  ({ value, onChange, placeholder, disabled, tabIndex, type, inputProps, children, ...props }, ref) => {
    return (
      <Wrapper ref={ref} disabled={disabled} {...props}>
        <InputField
          value={value}
          type={type}
          placeholder={placeholder}
          disabled={disabled}
          onChange={onChange}
          {...inputProps}
        />
        {children}
      </Wrapper>
    );
  }
);

TextField.displayName = 'TextField';

/** @component */
export default TextField;
