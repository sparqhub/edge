import React from 'react';
import styled, { css } from 'styled-components';

import { Elevation, Size } from 'types/helpers';
import { makeSpacing, borderRadius, elevation, makeSpace } from 'utils';

import Button from 'components/Button';
import StyledButton from 'components/Button/Button.helper';
import Icon from 'components/Icon';
import StyledInput from './Input.helper';

interface Props {
  disabled?: boolean;
  elevation?: Elevation;
  size?: Exclude<Size, 'xs' | 'xl'>;
}

const sizeMap = {
  s: css`
    font-size: 12px;
    padding: ${makeSpace(0.5)};
    & > ${Icon}:last-child {
      margin-left: ${makeSpace(0.5)};
      margin-right: 0;
    }
    & > ${StyledInput} + ${StyledButton} {
      margin-right: ${makeSpace(0.5)};
    }
    & > ${Icon}, & > ${StyledInput} + ${Icon} {
      margin-right: ${makeSpace(0.5)};
      margin-left: 0;
    }
  `,
  m: css`
    font-size: 14px;
    height: 36px;
    padding: ${makeSpacing(0.5, 1)};
    & > ${StyledButton}:last-child {
      margin-right: ${makeSpace(-0.5)};
      margin-left: ${makeSpace(0.5)};
    }
    & > ${Icon}:last-child {
      margin-left: ${makeSpace(1)};
      margin-right: 0;
    }
    & > ${StyledInput} + ${StyledButton} {
      margin-right: ${makeSpace(0.5)};
      margin-left: ${makeSpace(-0.5)};
    }
    & > ${StyledInput} + ${Icon} {
      margin-right: ${makeSpace(1)};
      margin-left: 0;
    }
  `,
  l: css`
    font-size: 18px;
    height: 50px;
    padding: ${makeSpacing(1.75, 2.5)};
    & > ${Icon}:last-child {
      margin-left: ${makeSpace(1)};
      margin-right: ${makeSpace(-1)};
    }
    & > ${StyledInput} + ${Icon} {
      margin-right: ${makeSpace(1)};
      margin-left: ${makeSpace(-1)};
    }
    & > ${StyledButton}:last-child {
      margin-right: ${makeSpace(-1.5)};
      margin-left: ${makeSpace(1)};
    }
    & > ${StyledInput} + ${StyledButton} {
      margin-right: ${makeSpace(1)};
      margin-left: ${makeSpace(-1.5)};
    }
  `
};

const WrapperStyled = styled.div<Props>`
  --bg-color: ${({ theme, disabled }) =>
    disabled ? theme.colors.input.default.disabled : theme.colors.input.default.default};
  --border-color: ${({ theme, disabled }) => (disabled ? 'transparent' : theme.colors.common.border.default)};
  display: inline-flex;
  align-items: center;
  flex-flow: row nowrap;
  ${({ size }) => sizeMap[size]}
  background: var(--bg-color, '#fff');
  border: 1px solid var(--border-color, transparent);
  box-sizing: border-box;
  ${borderRadius}
  ${elevation}
  &:hover {
    ${({ disabled }) =>
      !disabled &&
      css`
        --bg-color: ${({ theme }) => theme.colors.input.default.hover};
        --border-color: ${({ theme }) => theme.colors.common.border.hover};
      `}
  }
  &:focus-within {
    --border-color: ${({ theme }) => theme.colors.common.border.focus};
  }
  & > ${StyledButton} {
    box-shadow: none;
    border: none;
  }
  ${({ disabled }) =>
    disabled &&
    css`
      pointer-events: none;
      & > ${StyledButton} {
        background-color: transparent;
      }
    `}
`;

type LabelProps = React.ComponentPropsWithRef<typeof WrapperStyled>;

const sizeMaping: Record<Props['size'], Size> = {
  s: 'xs',
  m: 's',
  l: 'm'
};

const Wrapper: React.ForwardRefExoticComponent<LabelProps> = React.forwardRef<HTMLDivElement, LabelProps>(
  ({ children, ...props }, ref) => {
    return (
      <WrapperStyled {...props} ref={ref}>
        {React.Children.map(children, (child: React.ReactElement<any>) => {
          if (React.isValidElement(child) && (child.type === Icon || child.type === Button)) {
            return React.cloneElement(child as React.ReactElement<any, any>, {
              disabled: props.disabled,
              tabIndex: -1,
              size: sizeMaping[props.size]
            });
          }
          return child;
        })}
      </WrapperStyled>
    );
  }
);

Wrapper.defaultProps = {
  elevation: 0,
  size: 'm'
};

Wrapper.displayName = 'TextField';

export default Wrapper;
