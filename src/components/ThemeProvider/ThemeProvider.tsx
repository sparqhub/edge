import React, { FC, Fragment, useMemo } from 'react';
import { ThemeProvider as StyleProvider, DefaultTheme } from 'styled-components';
import merge from 'lodash.merge';

import { theme as defaultTheme, GlobalStyles } from 'utils/theme';

interface Props {
  theme?: DefaultTheme;
}

const ThemeProvider: FC<Props> = ({ children, theme }) => {
  const mergedTheme = useMemo(() => merge(defaultTheme, theme), [theme]);

  return (
    <StyleProvider theme={mergedTheme}>
      <Fragment>
        <GlobalStyles />
        {children}
      </Fragment>
    </StyleProvider>
  );
};

export default ThemeProvider;

const links = [
  'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900&display=swap&subset=cyrillic',
  'https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Sharp|Material+Icons+Round'
];

links.forEach(link => {
  const el = document.createElement('link');
  el.href = link;
  el.rel = 'stylesheet';
  document.head.appendChild(el);
});
