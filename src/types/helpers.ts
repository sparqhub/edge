export type SpacingValue = 'auto' | number;

export interface Padding {
  px?: SpacingValue;
  py?: SpacingValue;
}
export interface Margin {
  mx?: SpacingValue;
  my?: SpacingValue;
}

export type Elevation = 0 | 1 | 2 | 3 | 4 | 8 | 16 | 24;

export type Size = 'xs' | 's' | 'm' | 'l' | 'xl';
