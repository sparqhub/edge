import 'styled-components';

type ThemeComponentColor = {
  default: string;
  hover?: string;
  active?: string;
  focus?: string;
  disabled?: string;
};
type ThemeColor = {
  100?: string;
  200?: string;
  300?: string;
  400?: string;
  500?: string;
  600?: string;
  700?: string;
  800?: string;
  900?: string;
};

enum ButtonVariant {
  default = 'default',
  flat = 'flat',
  transparent = 'transparent',
  primary = 'primary',
  warning = 'warning',
  danger = 'danger'
}

type EdgeTheme = {
  colors: {
    gray?: ThemeColor;
    blue?: ThemeColor;
    yellow?: ThemeColor;
    red?: ThemeColor;
    common?: {
      border?: ThemeComponentColor;
      card?: ThemeComponentColor;
      [k: string]: ThemeComponentColor;
    };
    button: {
      [k in ButtonVariant]: ThemeComponentColor;
    };
    input: {
      default: ThemeComponentColor;
    };
    [k: string]: ThemeColor | Record<string, ThemeComponentColor>;
  };
  borderRadius: string;
  spacing: number;
  shadow: {
    '0': string;
    '1': string;
    '2': string;
    '3': string;
    4: string;
    8: string;
    16: string;
    24: string;
  };
  screen: {
    xs: string;
    s: string;
    m: string;
    l: string;
    xl: string;
  };
};

declare module 'styled-components' {
  // eslint-disable-next-line
  export interface DefaultTheme extends EdgeTheme {}
}
