import { css } from 'styled-components';
import { Padding, Margin, Elevation, SpacingValue } from 'types/helpers';

const pixelify = (space: SpacingValue, multiplier: number) =>
  typeof space === 'string' ? space : `${space * multiplier}px`;

export const makeSpace = (n: SpacingValue) => ({ theme: { spacing } }) => pixelify(n, spacing);

export const makeSpacing = (y: SpacingValue, x: SpacingValue) => ({ theme: { spacing } }) =>
  `${pixelify(y, spacing)} ${pixelify(x, spacing)}`;

export const padding = css<Padding>`
  padding: ${({ py = 0, px = 0 }) => makeSpacing(py, px)};
`;

export const margin = css<Margin>`
  margin: ${({ my = 0, mx = 0 }) => makeSpacing(my, mx)};
`;

export const elevation = css<{ elevation: Elevation }>`
  box-shadow: ${({ theme, elevation }) => theme.shadow[elevation]};
`;

export const borderRadius = css`
  border-radius: ${({ theme: { borderRadius } }) => borderRadius};
`;
