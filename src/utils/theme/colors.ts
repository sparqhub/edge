import { DefaultTheme } from 'styled-components';

const blue = {
  100: '#EBF8FF',
  200: '#BEE3F8',
  300: '#90CDF4',
  400: '#63B3ED',
  500: '#4299E1',
  600: '#3182CE',
  700: '#2B6CB0',
  800: '#2C5282',
  900: '#2A4365'
};

const gray = {
  100: '#F7FAFC',
  200: '#EDF2F7',
  300: '#E2E8F0',
  400: '#CBD5E0',
  500: '#A0AEC0',
  600: '#718096',
  700: '#4A5568',
  800: '#2D3748',
  900: '#1A202C'
};

const red = {
  100: '#FFF5F5',
  200: '#FED7D7',
  300: '#FEB2B2',
  400: '#FC8181',
  500: '#F56565',
  600: '#E53E3E',
  700: '#C53030',
  800: '#9B2C2C',
  900: '#742A2A'
};

const yellow = {
  100: '#FFECB3',
  200: '#FFE082',
  300: '#FFD54F',
  400: '#FFCA28',
  500: '#FFC107',
  600: '#FFB300',
  700: '#FFA000',
  800: '#FF8F00',
  900: '#FF6F00'
};

const common = {
  border: {
    default: gray[400],
    hover: gray[500],
    focus: gray[800]
  },
  card: {
    default: '#FFF'
  }
};

const button = {
  default: {
    default: '#FFFFFF',
    hover: gray[200],
    active: gray[100]
  },
  transparent: {
    default: 'transparent',
    hover: gray[200],
    active: gray[100]
  },
  flat: {
    default: gray[200],
    hover: gray[300],
    active: gray[200]
  },
  primary: {
    default: blue[600],
    hover: blue[700],
    active: blue[600]
  },
  warning: {
    default: yellow[600],
    hover: yellow[700],
    active: yellow[600]
  },
  danger: {
    default: red[600],
    hover: red[700],
    active: red[600]
  }
};

const input = {
  default: {
    default: '#FFFFFF',
    hover: '#FFFFFF',
    disabled: gray[100]
  }
};

const colors: DefaultTheme['colors'] = {
  red,
  blue,
  yellow,
  gray,
  common,
  button,
  input
};

export default colors;
