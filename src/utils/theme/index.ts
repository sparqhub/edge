import { createGlobalStyle, DefaultTheme } from 'styled-components';
import colors from './colors';
import core from './core';

export const theme: DefaultTheme = {
  colors,
  ...core
};

type StyleProps = { theme: typeof theme };

export const GlobalStyles = createGlobalStyle<StyleProps>`
  body {
    background-color: ${({ theme }) => theme.colors.gray[100]};
    color: ${({ theme }) => theme.colors.gray[900]};
    font: 400 14px/20px 'Source Sans Pro', sans-serif;
    margin: 0;
  }
`;
