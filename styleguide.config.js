const path = require('path');

module.exports = {
  styleguideDir: 'styleguide-docs',
  components: 'src/components/**/*.{ts,tsx}',
  ignore: ['**/*.spec.js', '**/components/**/*.helper.tsx'],
  sections: [
    {
      name: 'Introduction',
      content: 'docs/introduction.md'
    },
    {
      name: 'Documentation',
      sections: [
        {
          name: 'Installation',
          content: 'docs/installation.md'
        },
        {
          name: 'Live Demo',
          external: true,
          href: 'http://example.com'
        }
      ]
    },
    {
      name: 'Components',
      content: 'docs/ui.md',
      components: 'src/components/**/*.tsx',
      exampleMode: 'collapse', // 'hide' | 'collapse' | 'expand'
      usageMode: 'collapse' // 'hide' | 'collapse' | 'expand'
    }
  ],
  webpackConfig: {
    module: {
      rules: [
        {
          test: /\.(ts|js)x?$/,
          exclude: /node_modules/,
          include: [`${__dirname}/src`],
          loader: 'babel-loader'
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2|otf)$/,
          loader: 'file-loader'
        }
      ]
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx'],
      modules: ['node_modules', 'src'],
      alias: {
        '@sparqhub/edge': path.join(__dirname, './src/@sparqhub/edge')
      }
    }
  },
  getComponentPathLine(componentPath) {
    const name = path.dirname(componentPath).split('/');

    return `import { ${name[name.length - 1]} } from '@sparqhub/edge;`;
  },
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'src/components/ThemeProvider')
  },
  propsParser: require('react-docgen-typescript').withCustomConfig('./tsconfig.json', {
    skipPropsWithoutDoc: true,
    propFilter: prop => {
      if (prop.parent == null) {
        return true;
      }

      return prop.parent.fileName.indexOf('node_modules/@types/react') < 0;
    },
    componentNameResolver: (exp, source) => {
      return exp.getName() === 'StyledComponentClass' && getDefaultExportForFile(source);
    }
  }).parse
};
