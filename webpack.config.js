const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require('compression-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const pkg = require('./package.json');

const config = {
  entry: {
    index: `${__dirname}/src/@sparqhub/edge`
  },
  output: {
    filename: 'index.js',
    library: 'edge',
    path: `${__dirname}/dist`,
    libraryTarget: 'umd',
    globalObject: 'this'
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        include: [`${__dirname}/src`],
        loader: 'babel-loader'
      },
      {
        test: /\.(png|jpg|svg|woff)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      }
    ]
  },
  stats: 'minimal',
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    modules: ['node_modules', 'src']
  },
  externals: Object.keys(pkg.peerDependencies).reduce((res, key) => ((res[key] = key), res), {}),
  plugins: [new WebpackCleanupPlugin(), new CompressionPlugin(), new BrotliPlugin()]
};

const isDev = Boolean(process.env.WATCH);
if (isDev) {
  config.mode = 'development';
  config.watch = true;
} else {
  config.mode = 'production';
  config.optimization = {
    minimizer: [
      new TerserPlugin({
        parallel: true
      })
    ]
  };
}

if (process.env.REPORT) {
  config.plugins.push(
    new BundleAnalyzerPlugin({
      analyzerMode: 'static'
    })
  );
}

module.exports = config;
